---
layout: markdown_page
title: "OKRs and LatticeHQ"
---


Our Objectives & Key Results (OKRs) are all to be found in [LatticeHQ](https://gitlab.latticehq.com).

Use this page to find or add tips, tricks, and guidance on OKRs generally and on how to use LatticeHQ specifically.

### Child Goals vs. Key Results

The key difference between Key Results and Child Goals is ownership: you will own your own Key Results, whereas child goals can be owned by someone else on your team. If you make it a child goal and someone on your team owns it, it'll show up in their personal goals tab, and they'll be responsible for updating it etc. It can still be connected to your goal as a child goal, but you won't be the one owning and updating it.

### Changing your Name in Lattice

Some employees do not go by their legal first names. As Lattice defaults to pulling this data from BambooHR, the name field may need to be updated. You can do that here: https://gitlab.latticehq.com/settings/user
